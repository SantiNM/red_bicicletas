class Bicicleta {
  constructor(id, color, modelo, ubicacion) {
    this.id = id;
    this.color = color;
    this.modelo = modelo;
    this.ubicacion = ubicacion;
  }
  toString() {
    return "id " + this.id + " | color: " + this.color;
  }
  static add(aBici) {
    Bicicleta.allBicis.push(aBici);
  }

  static findById(aBiciId) {
    var aBici = Bicicleta.allBicis.find((x) => x.id == aBiciId);
    if (aBici) {
      return aBici;
    } else {
      throw new Error(`No existe bicicleta con el id ${aBiciId}`);
    }
  }

  static removeById(aBiciId) {
    for (var i = 0; i < Bicicleta.allBicis.length; i++) {
      if (Bicicleta.allBicis[i].id == aBiciId) {
        Bicicleta.allBicis.splice(i, 1);
        break;
      }
    }
  }
}

Bicicleta.allBicis = [];

module.exports = Bicicleta;
