let Bicicleta = require("../../models/bicicletas");
let request = require("request");
let server = require("../../bin/www");

beforeEach(() => {
  Bicicleta.allBicis = [];
});
describe("Bicicleta API", () => {
  describe("GET BICICLETAS /", () => {
    it("Status 200", (done) => {
      expect(Bicicleta.allBicis.length).toBe(0);

      let a = new Bicicleta(1, "rojo", "urbana", [-34.6012424, -58.3861497]);
      let b = new Bicicleta(2, "blanca", "urbana", [-34.596932, -58.3808287]);
      Bicicleta.add(a);
      Bicicleta.add(b);

      request.get(
        "http://localhost:3000/api/bicicletas",
        (error, response, body) => {
          expect(response.statusCode).toBe(200);
          expect(Bicicleta.allBicis.length).toBe(2);
          done();
        }
      );
    });
  });

  describe("POST BICICLETAS /create", () => {
    it("Status 200", (done) => {
      var headers = { "content-type": "application/json" };
      var aBici =
        '{"id": 1, "color": "rojo", "modelo": "urbano", "lat": -34.6012424, "lgn": -58.3861497}';

      request.post(
        {
          headers: headers,
          url: "http://localhost:3000/api/bicicletas/create",
          body: aBici,
        },
        (error, response, body) => {
          expect(response.statusCode).toBe(200);
          expect(Bicicleta.findById(1).color).toBe("rojo");
          expect(Bicicleta.allBicis.length).toBe(1);
          done();
        }
      );
    });
  });

  describe("DELETE BICICLETAS /delete", () => {
    it("Status 204", (done) => {
      expect(Bicicleta.allBicis.length).toBe(0);

      let a = new Bicicleta(1, "rojo", "urbana", [-34.6012424, -58.3861497]);
      let b = new Bicicleta(2, "blanca", "urbana", [-34.596932, -58.3808287]);
      Bicicleta.add(a);
      Bicicleta.add(b);

      expect(Bicicleta.allBicis.length).toBe(2);

      var headers = { "content-type": "application/json" };
      var id = '{"id": 2}';

      request.delete(
        {
          headers: headers,
          url: "http://localhost:3000/api/bicicletas/delete",
          body: id,
        },
        (error, response, body) => {
          expect(response.statusCode).toBe(204);
          expect(Bicicleta.allBicis.length).toBe(1);
          done();
        }
      );
    });
  });

  describe("UPDATE BICICLETAS /update", () => {
    it("Status 200", (done) => {
      let a = new Bicicleta(1, "rojo", "urbana", [-34.6012424, -58.3861497]);
      let b = new Bicicleta(2, "blanca", "urbana", [-34.596932, -58.3808287]);
      Bicicleta.add(a);
      Bicicleta.add(b);

      var headers = { "content-type": "application/json" };
      var biciId = '{"id": 2, "color": "violeta"}';

      request.post(
        {
          headers: headers,
          url: "http://localhost:3000/api/bicicletas/update",
          body: biciId,
        },
        (error, response, bosy) => {
          expect(response.statusCode).toBe(200);
          expect(Bicicleta.findById(2).color).toBe("violeta");
          done();
        }
      );
    });
  });
});
