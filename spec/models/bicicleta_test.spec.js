let Bicicleta = require("../../models/bicicletas");

beforeEach(() => {Bicicleta.allBicis = []})
describe("Bicicleta.allBicis", () => {
  it("comienza vacía", () => {
    expect(Bicicleta.allBicis.length).toBe(0);
  });
});

describe(Bicicleta.add, () => {
  it("agrega una bicicleta", () => {
    expect(Bicicleta.allBicis.length).toBe(0);

    let a = new Bicicleta(1, "rojo", "urbana", [-34.6012424, -58.3861497]);
    Bicicleta.add(a)
    
    expect(Bicicleta.allBicis.length).toBe(1);
    expect(Bicicleta.allBicis[0]).toBe(a);
  });
});

describe("Bicicleta.findById", () => {
    it("Busca una bicicleta por su id", () =>{
        expect(Bicicleta.allBicis.length).toBe(0);

        let a = new Bicicleta(1, "rojo", "urbana", [-34.6012424, -58.3861497]);
        let b = new Bicicleta(2, "blanca", "urbana", [-34.596932, -58.3808287]);
        
        Bicicleta.add(a)
        Bicicleta.add(b)

        let targetBici = Bicicleta.findById(1)
        expect(targetBici.id).toBe(a.id)
        expect(targetBici.color).toBe(a.color)
        expect(targetBici.modelo).toBe(a.modelo)
    })
})

describe("Bicicletas.removeById.", () => {
    it("elimina una bici por su id", () =>{
        expect(Bicicleta.allBicis.length).toBe(0);

        let a = new Bicicleta(1, "rojo", "urbana", [-34.6012424, -58.3861497]);
        let b = new Bicicleta(2, "blanca", "urbana", [-34.596932, -58.3808287]);
        
        Bicicleta.add(a)
        Bicicleta.add(b)

        expect(Bicicleta.allBicis.length).toBe(2);
        Bicicleta.removeById(1)
        expect(Bicicleta.allBicis.length).toBe(1);
    })
})
